package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MarsRoverTest {
	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	
	@Before
	public void setup(){
		planetX = 10;
		planetY = 10;
		planetObstacles = new ArrayList<>();
	}

	@Test
	public void marsRoverShouldNotThrowException() throws MarsRoverException{
		new MarsRover(planetX, planetY, planetObstacles);
	}
	
	@Test(expected=MarsRoverException.class)
	public void marsRoverShouldThrowExceptionObstacleWithWhiteSpace() throws MarsRoverException{
		planetObstacles.add("(4 ,7)");
		
		new MarsRover(planetX, planetY, planetObstacles);
	}
	
	@Test(expected=MarsRoverException.class)
	public void marsRoverShouldThrowExceptionOn0x0Planet() throws MarsRoverException{
		new MarsRover(0, 0, planetObstacles);
		
		new MarsRover(planetX, planetY, planetObstacles);
	}
	
	@Test
	public void planetShouldContainsObstacleIn47() throws MarsRoverException{
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		planetObstacles.add("(4,7)");
		assertTrue(rover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	public void roverPostionShouldBe00() throws MarsRoverException{
		
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	public void roverPostionShouldTurnRight() throws MarsRoverException{
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	public void roverPostionShouldTurnLeft() throws MarsRoverException{
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	public void roverPostionShouldMoveForward() throws MarsRoverException{	
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		assertEquals("(0,1,N)", rover.executeCommand("f"));
	}

	@Test
	public void roverPostionShouldBe76N() throws MarsRoverException{
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("r");
		assertEquals("(1,2,E)", rover.executeCommand("f"));
	}
	
	@Test
	public void roverPostionShouldMoveBackward() throws MarsRoverException{
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		rover.executeCommand("f");
		assertEquals("(0,0,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void roverSholudBeIn22N() throws MarsRoverException{
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		rover.executeCommand("f");
		rover.executeCommand("r");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("l");
		rover.executeCommand("f");
		rover.executeCommand("f");
		assertEquals("(2,2,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void roverShouldMoveWithCombinedCommandIn22E() throws MarsRoverException{
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	
	@Test
	public void roverShouldMoveBackwardInASphericPlanet() throws MarsRoverException{
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		assertEquals("(0,9,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void roverShouldMoveForwardInASphericPlanet() throws MarsRoverException{	
		MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
		assertEquals("(0,0,N)", rover.executeCommand("ffffffffff"));
	}
}
