package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	private int planetX;
	private int planetY;
	private String position;
	private final List<Character> directions = new ArrayList<>();
	private List<String> planetObstacles = new ArrayList<>();

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		if(planetX != 0 || planetY != 0) {
			this.planetX = planetX-1;
			this.planetY = planetY-1;
		}else {
			throw new MarsRoverException();
		}
		
		for(int i = 0; i < planetObstacles.size(); i++) {
			if(planetObstacles.get(i).contains(" ")) {
				throw new MarsRoverException();
			}
		}
		this.directions.add('N');
		this.directions.add('E');
		this.directions.add('S');
		this.directions.add('W');
		this.planetObstacles = planetObstacles;
		this.position = "(0,0,N)";
		}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		String strPosition = "(";
		
		strPosition = strPosition + x + "," + y + ")";
		for(int i = 0; i < planetObstacles.size(); i++) {
			if(planetObstacles.get(i).equals(strPosition)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		if(commandString.length() > 1) {
			char []commandArray = commandString.toCharArray();
			for(int i = 0; i < commandArray.length; i++) {
				position = executeSingleCommand(String.valueOf(commandArray[i]));
			}
			return position;
		}else {
			return executeSingleCommand(commandString);
		}
		
	}

	private String executeSingleCommand(String commandString) throws MarsRoverException {
		char direction = position.charAt(5);
		switch (commandString){
		case "":
			return position;
		case "r":
			return turnRight(direction);
		case "l":
			return turnLeft(direction);
		case "f":
			return moveForward(direction);
		case "b":
			return moveBackward(direction);
		default:
			throw new MarsRoverException();
		}
	}

	private String moveBackward(char direction) throws MarsRoverException {
		char []positionArray = position.toCharArray();
		int x = Character.getNumericValue(position.charAt(1));
		int y = Character.getNumericValue(position.charAt(3));
		
		switch (direction) {
		case 'N':
			if(y == 0) {
				positionArray[3] = (char)(planetY + 48);
			}else {
				positionArray[3] = (char)(y - 1 + 48);
			}
			position = String.valueOf(positionArray);
			return position;
		case 'E':
			if(x == 0) {
				throw new MarsRoverException();
			}
			positionArray[1] = (char)(x - 1 + 48);
			position = String.valueOf(positionArray);
			return position;
		case 'S':
			if(y == planetY) {
				throw new MarsRoverException();
			}
			positionArray[3] = (char)(y + 1 + 48);
			position = String.valueOf(positionArray);
			return position;
		case 'W':
			if(x == planetX) {
				throw new MarsRoverException();
			}
			positionArray[1] = (char)(x + 1 + 48);
			position = String.valueOf(positionArray);
			return position;
		default:
			throw new MarsRoverException();
		}
	}

	private String moveForward(char direction) throws MarsRoverException {
		char []positionArray = position.toCharArray();
		int x = Character.getNumericValue(position.charAt(1));
		int y = Character.getNumericValue(position.charAt(3));
		
		switch (direction) {
		case 'N':
			if(y == planetY) {
				positionArray[3] = (char)(0 + 48);
			}else {
				positionArray[3] = (char)(y + 1 + 48);
			}
			position = String.valueOf(positionArray);
			return position;
		case 'E':
			if(x == planetX) {
				throw new MarsRoverException();
			}
			positionArray[1] = (char)(x + 1 + 48);
			position = String.valueOf(positionArray);
			return position;
		case 'S':
			if(y == 0) {
				throw new MarsRoverException();
			}
			positionArray[3] = (char)(y - 1 + 48);
			position = String.valueOf(positionArray);
			return position;
		case 'W':
			if(x == 0) {
				throw new MarsRoverException();
			}
			positionArray[1] = (char)(x - 1 + 48);
			position = String.valueOf(positionArray);
			return position;
		default:
			throw new MarsRoverException();
		}
	}

	private String turnLeft(char direction) {
		int newDirectionIndex;
		newDirectionIndex = directions.indexOf(direction) - 1;
		if(newDirectionIndex == -1)
			newDirectionIndex = 3;
		position = position.replace(direction, directions.get(newDirectionIndex));
		return position;
	}

	private String turnRight(char direction) {
		int newDirectionIndex;
		newDirectionIndex = directions.indexOf(direction) + 1;
		if(newDirectionIndex == 4)
			newDirectionIndex = 0;
		position = position.replace(direction, directions.get(newDirectionIndex));
		return position;
	}

}
